import React,{useContext} from 'react'

import {Navbar,Nav} from 'react-bootstrap'

import {NavLink} from 'react-router-dom'

import UserContext from 'userContext'



export default function NavBar() {

	
	const {user} = useContext(UserContext)
	

	return (

		
		  <Navbar bg="dark" variant="dark" expand="lg">
		    <Navbar.Brand>Magic-Money</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		    <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="mr-auto">
		      <Nav.Link as={NavLink}to="/home">Home</Nav.Link>
		      <Nav.Link as={NavLink}to="/categories">Categories</Nav.Link>
		      <Nav.Link as={NavLink}to="/entries">Entries</Nav.Link>
		      {
		      	user.email
		      	?
		      	<Nav.Link as={NavLink}to="/logout">Logout</Nav.Link>
		      	:
		      	<>
		      	<Nav.Link as={NavLink}to="/register">Register</Nav.Link>
		      	<Nav.Link as={NavLink}to="/login">Login</Nav.Link>

		      	</>

		      }
				
		    </Nav>
		    </Navbar.Collapse>
		  </Navbar>
		
	)
}