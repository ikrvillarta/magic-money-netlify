import React,{useState,useEffect,useContext} from 'react'

import {Redirect} from 'react-router-dom'
import {Table,Form,Button} from 'react-bootstrap'
import UserContext from 'userContext'


export default function Entries(){

	const {user} = useContext(UserContext)
	const [category,setCategory] = useState([])
	const [amount, setAmount] = useState([])
	const [type, setType] = useState("")
	const [addCategory,setAddCategory] = useState([])
	const [categoryElements,setCategoryElements]=useState([])
	const [viewEntries,setViewEntries] = useState([])



	useEffect(()=>{
		setCategoryElements(addCategory.map(elements=>{
			console.log(addCategory)
			return (
				
				<option key={elements._id}>{elements.name}</option>	
				
				)
		}))

	},[addCategory])

	useEffect(()=>{
		fetch('https://still-earth-79791.herokuapp.com/api/categories/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			if (data.auth === "failed"){
				<Redirect to="/login"/>
			}
			else{
			setAddCategory(data.filter(categories=>{
				return(categories.type === type)
			

			}))
		}

		})
		
		
	},[type])
	
	

	useEffect(()=>{
		fetch('https://still-earth-79791.herokuapp.com/api/entries/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			if (data.auth==="failed"){
				alert("Login First")
			}
			else{
			setViewEntries(data.map(entries=>{
				
				return (

					







					<tr>
						<td>{entries.category}</td>
						<td>{entries.type}</td>
						<td>{entries.amount}</td>
					</tr>

				)
			}))
		}
		})

	},[])
			

	function addEntries(e){
		e.preventDefault()
		

		fetch('https://still-earth-79791.herokuapp.com/api/entries/',{
			method: 'POST',
			headers: {
				"Content-Type":"application/json",
				"Authorization":`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				type:type,
				category:category,
				amount:amount
			})
		})
		.then(res=>res.json())
		.then(data=>{
			
			alert("Entry Successfully Added!")
		})
		setCategory("")
		setType("")
		setAmount("")
	}
	
	
		
	
	return (
		
		user.email
		?

			<div>

				<h1 className="text-center">

				{type==="" ? "Choose Type" : type}
				</h1>
					<Form onSubmit={e=>addEntries(e)} className="content">
					<Form.Group>
						<Form.Label>Type</Form.Label>
						<Form.Control as="select" value={type} onChange={e=>{setType(e.target.value)}} required>
							<option value="">Choose</option>
							<option>Income</option>
							<option>Expenses</option>
							
							
						</Form.Control>
					</Form.Group>

					<Form.Group>
						<Form.Label>Category</Form.Label>
						<Form.Control as="select" value={category} onChange={e=>{setCategory(e.target.value)}} required>
							<option value="">Choose</option>
							{categoryElements}	
						</Form.Control>
					</Form.Group>
						<Form.Group>
						<Form.Label>Amount</Form.Label>
						<Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={e=>{setAmount(e.target.value)}} required/>
						
					</Form.Group>
					<Button variant="dark" type="submit">Submit</Button>
					<h1 className="text-center">Entries</h1>

					

					








					<Table striped bordered hover>
						<thead className="content">
							<tr>
								<th>Name</th>
								<th>Type</th>
								<th>Amount</th>
								
							</tr>
						</thead>
						
						<tbody>
							{viewEntries}
						</tbody>
					</Table>

					</Form>

					
			</div>
			:
			<Redirect to="/login"/>

		
		)
}
