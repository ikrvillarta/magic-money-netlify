import React,{useState,useEffect,useContext} from 'react'

import {Redirect} from 'react-router-dom'
import {Table,Form,Button} from 'react-bootstrap'
import UserContext from 'userContext'

export default function CreateCategories(){

	const {user} = useContext(UserContext)
	const [name,setName] = useState("")
	const [type,setType] = useState("")
	const [update,setUpdate] = useState([])
	
	useEffect(()=>{
		fetch('https://still-earth-79791.herokuapp.com/api/categories/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			if (data.auth === "failed"){
				alert("Login First")
			}
			else{
			setUpdate(data.map(categories=>{

				return (

					<tr>
						
						<td>{categories.name}</td>
						<td>{categories.type}</td>
						
					</tr>	

				)
			}))
			}
		})

	},[])

	
	
	function addCategories(e){

		e.preventDefault()

		fetch('https://still-earth-79791.herokuapp.com/api/categories/',{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name:name,
				type:type
			})
		})
		.then(res=>res.json())
		.then(data=>{
			
			
			
			
				alert("Category Successfully Added!")
			

			
		})

		setName("")
		setType("")
	}
	return (

		user.email
		?
		<div>
			<h1 className="text-center">Add Category</h1>
				<Form onSubmit={e=>addCategories(e)}>
					<Form.Group>
						<Form.Label>Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Category Name" value={name} onChange={e=>{setName(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Type</Form.Label>
						<Form.Control as="select" id="others" value={type} onChange={e=>{setType(e.target.value)}} required>
							<option>Choose</option>
							<option>Income</option>
							<option>Expenses</option>
							

						</Form.Control>
					</Form.Group>
					
						<Button variant="dark" type="submit">Submit</Button>
					
		
				</Form>
				<>
				<h1 className="text-center mt-5 mb-5">Categories</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							
							<th className="text-center">Name</th>
							<th className="text-center">Type</th>
						</tr>
					</thead>
					<tbody>
						{update}
					</tbody>
				</Table>
				
				</>
		</div>
		
		:
		<Redirect to="/login"/>
			
		
		
	)
}