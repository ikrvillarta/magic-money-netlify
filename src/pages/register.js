import React,{useState,useEffect} from 'react'
import {Form,Button} from 'react-bootstrap'



export default function Register(){

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")

	
	const [isActive,setIsActive] = useState(true)
	

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)){

			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[firstName,lastName,email,password,confirmPassword])

	function registerUser(e){

		e.preventDefault()

		fetch('https://still-earth-79791.herokuapp.com/api/users/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				confirmPassword: confirmPassword

			})

		})
		.then(res=> res.json())
		.then(data=>{
			console.log(data)
				alert("Registered Successfully!")
		})

		//clear out the states to their initial values
		setFirstName("")
		setLastName("")
		setEmail("")
		setPassword("")
		setConfirmPassword("")

	}
	return (
		<>
			<div>
				<h3 className="text-center" >Register</h3>
				<Form onSubmit={e=>registerUser(e)}>
					<Form.Group>
						<Form.Label className="dark">First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e=>{setFirstName(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e=>{setLastName(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e=>{setPassword(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}} required/>
					</Form.Group>
					{
						isActive
						?
						<Button variant="dark" type="submit">Submit</Button>
						:
						<Button variant="dark" disabled>Submit</Button>
					}
					
				</Form>
			</div>
				
		</>


	)
}