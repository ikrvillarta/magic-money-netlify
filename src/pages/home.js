import React from 'react'

import Highlights from 'components/Highlights'

export default function Home() {
	
	return (
		<>
			<Highlights />
		</>
	)
}