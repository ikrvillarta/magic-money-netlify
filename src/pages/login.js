import React,{useState,useEffect,useContext} from 'react'
import {Form,Button} from 'react-bootstrap'



import {Redirect} from 'react-router-dom'

import UserContext from 'userContext'

export default function Login(){

	const {user,setUser} = useContext(UserContext) 
	

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(true)
	

	useEffect(()=>{
		if (password !== "" && email !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){

		e.preventDefault()

		

		fetch('https://still-earth-79791.herokuapp.com/api/users/login',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res=> res.json())
		.then(data=>{
			
			if (data.accessToken) {
					localStorage.setItem('token',data.accessToken)
				
					fetch('https://still-earth-79791.herokuapp.com/api/users/',{
						headers: {
							Authorization: `Bearer ${data.accessToken}`
						}
					})
					.then(res=> res.json())
					.then(data=>{
						localStorage.setItem('email',data.email)
						localStorage.setItem('isAdmin',data.isAdmin)


						
						setUser({
							email: data.email,
							password: data.password
						})
						

					})
					
					alert("You are already logged in")
					
					      
				
				} else {

					alert("Login Failed")
				}

			})
			
		setEmail("")
		setPassword("")
		

	}
	return(
			user.email
			?
			<Redirect to="/categories" />
			:
			<div>
				<h3 className="text-center">Login</h3>

				<Form onSubmit={e=>loginUser(e)}>
					<Form.Group>
						<Form.Label>Email</Form.Label>
							<Form.Control type="Email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
							<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e=>{setPassword(e.target.value)}} required/>
					</Form.Group>
					{
						isActive
						?
						<Button variant="dark" type="submit">Submit</Button>
						:
						<Button variant="dark" disabled>Submit</Button>
					}
				</Form>

			</div>

		)
	}

