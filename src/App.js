import {useState} from 'react'

import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

import './App.css'

import {UserProvider} from './userContext'

import NavBar from 'components/NavBar'
import Home from 'pages/home'
import Register from 'pages/register'
import Login from 'pages/login'
import Logout from 'pages/logout'
import CreateCategories from 'pages/categories'
import Entries from 'pages/entries'



function App (){

const [user,setUser] = useState ({

    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'


})
const unsetUser = ()=>{
    localStorage.clear()
  }

  return (
    <>  
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <NavBar fixed="top" />
            <Container>
              <Switch>

                <Route exact path="/home" component={Home} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/logout" component={Logout} />
                <Route exact path="/categories" component={CreateCategories} />
                <Route exact path="/entries" component={Entries} />
              </Switch>
            </Container>
        </Router>
        
      </UserProvider>
    </>
  )
}

export default App;

